---
marp: true
theme: uncover
footer: 'NetDevOps Days - London 2023-06'

---

## **Explaining the business value of network automation**

#### Or why I succeeded the second time around

Pete Crocker
[pete.crocker@ipfabric.io](mailto:pete.crocker@ipfabric.io)

![bg](bg3.png)

---

## Audience Participation

<!-- First, let’s see where network automation is at. So we can understand who’s in the room today:
* Who’s a network engineer?
* Who’s a developer?
* Zero, One hand raised, or two hands? How much automation are you doing now? None, basics, advanced?
* Who’s doing this in your company from the grassroots? As in you started the initiative yourself?
* Who’s got management buy-in?
  * Really? Who gets time to step away from firefighting?
* Who’s doing artisanal coding?
  * Using frameworks, like ansible?
  * Using commercial tools? -->

![bg](bg3.png)

---

## Backstory

* Network engineer @ large SP in the mid-90s
* 2014 - DevOps sink or swim

<!-- My first professional love! Sun Sparc 20, Livingston Portmaster & dialup modems. -->

<!-- Really got in to the dev and automation world in 2014 when I was at a startup and was surrounded by 100% hardcore developers doing hardcore modern development. Everything CI-CD, full test coverage. -->

![bg right:35%](sun.jpeg)

---

## New $dayjob

* 2020: Network automation engineer
* Built automation for myself
* Never time for enablement

<!-- Hired to do automation in to a typical network engineering team doing very little scripting, much less automation. Google sheets (excel), Google Docs, draw.io. Nothing kept accurate. -->

<!-- Tried to push for team enablement, team never had time, some had no interest, and there was no management backing to make the space for those that did have interest. -->

![bg](bg3.png)

---

## Lesson 1

#### _Speak their language_

* I'd done this journey multiple times before!
* I didn’t speak the language of the target audience

<!-- I’d helped other companies through this journey, but didn’t properly think and plan how to take my current company through this journey. -->

<!-- It's common for us to forget others haven't learned what you've learned, and how difficult it was. -->

![bg](bg3.png)

---

## Their view of acceptable automation:

read-only chatbot

![bg](bg3.png)

---

## My view:

* 100% code in gitlab
* ansible tower
* CI pipelines
* Continuous validation

![bg right w:600](all.jpg)

---

## My world

* Firefighting and “being responsive” was more valuable than automation
* Just login and add the VLAN
* Business value of automation wasn't clear to leaders

<!-- What should I have done?
* [Start with why](https://simonsinek.com/books/start-with-why/)
  * The “why” here being what’s the value to the business?
* Have a plan
  * Not just “automate”
  * Build up team skills
  * Metrics & timelines
  * How it aligns with business objectives -->

![bg](bg3.png)

---

## Lesson 2:
## "Hit by a bus"

![bg right w:400](hospital.jpeg)

---

* Morning of greenfield DC turnup
* ~~git commit; git push~~
* No documented action plan in advance!
* Effect: Special snowflakes on day 0

<!-- Wrong VLAN ID. Overruled on correcting it. Config consistency wasn’t important. Don’t interrupt the business was top priority -->

<!-- Why config consistency? We were hiring ops people. We were rapidly creating new DCs. Simplify so easier to troubleshoot and maintain. Path to automating more. -->

![bg right w:400](git.jpg)

---

## 2nd time around

* Different audience
* Different business need
* Different take-up

<!-- This time around, I didn't have to make the business case about automation. It was a mandatory part of the plan. That said... -->

![bg](bg3.png)

---

## Lesson 3:
#### _Know your limits_

* I am not a developer
* Test it!

<!-- Took me months to write a 3000 line python script. Pairing a developer with a network engineer would have resulted in much quicker code, but better quality code

* Also, I didn’t write a single line of python for months, then had a job interview, learned it was a live coding interview when I showed up. Failed so miserably it was embarrassing for me and the person interviewing me. One of the very few times I felt like I failed an interview, and MASSIVE impostor syndrome still to this day!
* Just because you can, doesn’t mean you should. Either personally (i’ll never be a developer), or from a wider company objective standpoint -->

<!-- Build and test and learn in a simulation!

* Not just code testing, automation testing
* [NVIDIA Air](https://www.nvidia.com/en-us/networking/ethernet-switching/air/)
* [netlab](https://netsim-tools.readthedocs.io/en/latest/index.html)
* I learned automation step ordering, how to validate, and run failure tests -->

![bg](bg3.png)

---

## Cool Techie Stuff

![bg](bg3.png)

---

## Bootstrap

* Ansible server
* DHCP

![bg right w:600](rpi.jpeg)

---

## Why ansible?

* Lingua franca between teams
* Maintainability
* Cool kids do Python but again: **business goals**

![bg right:35% w:200](ansible.png)

---

## Docs as code

* Cabling Map
* LLDP verification
* NetBox data population

<!-- Cabling info in just the format the DC team needed it -->

![bg right w:500](dac.png)

---

## NetBox

* Greenfield, so the code was the SoT
* Ansible populated NetBox
* SoT for servers, VMs, apps

![bg right:35% w:200](netbox.png)

---

## Final Thoughts

* Network automation is hard.
* Start small. Build on proven value.
* Community helps.

<!-- The more you learn, the more you learn the complexity of it. It’s not spinning up nginx on one VM, your affects can ripple to the far reaches of the network -->

<!-- For those of you earlier on, without much buy-in, you need to show the value. Start small. Start read-only. Docs, gathering inventory. Make sure it adds value to the business, and make sure others see that value. I went in too hardcore, and didn’t show incremental value that could be built on. -->

<!-- Community, sharing and learning from each other is important. That’s why I’m here. -->

![bg](bg3.png)
